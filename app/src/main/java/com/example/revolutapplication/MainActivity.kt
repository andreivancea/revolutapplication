package com.example.revolutapplication

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val itemsAdapter = RateItemAdapter()
    private var currencyChangeDisposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_rates.adapter = itemsAdapter
        rv_rates.layoutManager = LinearLayoutManager(this)
        rv_rates.setItemViewCacheSize(20)
        rv_rates.setHasFixedSize(true)
        (rv_rates.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rv_rates.onScrollStateChangeListener = object : CurrencyRecyclerView.OnScrollStateChangeListener {

            override fun scrollStateChanged(isScrolling: Boolean) {
                if (isScrolling) {
                    rv_rates.hideKeyboard()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        progress_loader.visibility = VISIBLE
        monitorCurrencyChanges(true)
    }

    override fun onPause() {
        super.onPause()
        monitorCurrencyChanges(true)
    }

    private fun monitorCurrencyChanges(monitor: Boolean) {
        currencyChangeDisposable?.dispose()
        if (monitor) {
            currencyChangeDisposable = CurrencyApiGateway.requestRates().subscribe({
                it?.let {
                    if (!rv_rates.isScrolling) {
                        itemsAdapter.updateItemsRate(it)
                        if (progress_loader.visibility == VISIBLE) progress_loader.visibility = GONE
                    }
                }
            }, {
                it.printStackTrace()
            })
        }
    }
}
