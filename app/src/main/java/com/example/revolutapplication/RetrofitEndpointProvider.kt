package com.example.revolutapplication

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


interface RevolutRatesApi {

    @GET("latest")
    @Headers("Content-Type:application/json")
    fun getRates(@Query("base") base: String = "EUR"): Observable<RatesResponse>
}

class RetrofitEndpointProvider {

    companion object {

        fun getRatesApi(): RevolutRatesApi {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://revolut.duckdns.org")
                .build()
                .create(RevolutRatesApi::class.java)
        }
    }


}