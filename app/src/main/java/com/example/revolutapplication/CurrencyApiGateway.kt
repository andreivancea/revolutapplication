package com.example.revolutapplication

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class CurrencyApiGateway {

    companion object {

        fun requestRates(): Observable<List<Pair<String, Double>>?> =
            Observable.interval(1, TimeUnit.SECONDS)
                .flatMap {
                    RetrofitEndpointProvider.getRatesApi().getRates().onErrorReturn { RatesResponse() }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.rates == null) return@map ArrayList<Pair<String, Double>>()
                    val ratesJsonObject = it.rates
                    val keys = ratesJsonObject.keySet()

                    val ratesList: MutableList<Pair<String, Double>> = ArrayList()
                    ratesList.add(Pair(it.base, 1.0))

                    for (key in keys) {
                        ratesList.add(Pair(key, ratesJsonObject.get(key).asDouble))
                    }
                    return@map ratesList.toList()

                }
    }
}
