package com.example.revolutapplication

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rate_item.view.*
import java.text.DecimalFormat

class RateItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private var lastSelectionIndex: Int = 0
    var itemChangedCallback: ItemChangedCallback? = null
    var itemFocusedCallback: ItemFocusedCallback? = null

    var itemPosition: Int = -1

    fun bind(rateItem: RateItemInfo, position: Int) {
        itemPosition = position

        with(itemView) {

            tv_currency.text = rateItem.name
            val currencyFormatter = DecimalFormat("#.###")
            et_amount.setText(if (rateItem.value == 0.0) "" else currencyFormatter.format(rateItem.value))
            et_amount.imeOptions = EditorInfo.IME_ACTION_DONE
            et_amount.filters = arrayOf(InputFilter.LengthFilter(19))
            et_amount.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    et_amount.hideKeyboard()
                    return@OnEditorActionListener true
                }
                return@OnEditorActionListener false
            })
            et_amount.setOnFocusChangeListener { _: View, focused: Boolean ->
                if (focused) {
                    itemFocusedCallback?.onItemFocused(this@RateItemViewHolder)
                    lastSelectionIndex = et_amount.selectionStart
                }
            }

            et_amount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(editable: Editable?) {
                    if (itemPosition == 0) {
                        rateItem.value = if (editable.toString().isEmpty()) 0.0 else editable.toString().toDouble()
                        itemChangedCallback?.onItemChanged(rateItem.value)
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

            })
        }
    }

    fun clearCallbacks() {
        itemChangedCallback = null
    }

    interface ItemChangedCallback {
        fun onItemChanged(value: Double)
    }

    interface ItemFocusedCallback {
        fun onItemFocused(holder: RateItemViewHolder)
    }
}
