package com.example.revolutapplication

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

class CurrencyRecyclerView(context: Context, attrs: AttributeSet) : RecyclerView(context, attrs) {

    var isScrolling: Boolean = false
    var onScrollStateChangeListener: OnScrollStateChangeListener? = null

    override fun onScrollStateChanged(state: Int) {
        when (state) {
            SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING -> {
                isScrolling = true
                onScrollStateChangeListener?.scrollStateChanged(true)
            }
            SCROLL_STATE_IDLE -> {
                isScrolling = false
                onScrollStateChangeListener?.scrollStateChanged(false)
            }
        }
        super.onScrollStateChanged(state)
    }

    interface OnScrollStateChangeListener {

        fun scrollStateChanged(isScrolling: Boolean)

    }
}
