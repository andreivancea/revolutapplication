package com.example.revolutapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RateItemAdapter : RecyclerView.Adapter<RateItemViewHolder>(), RateItemViewHolder.ItemChangedCallback,
    RateItemViewHolder.ItemFocusedCallback {

    private val holders = ArrayList<RateItemViewHolder>()
    private var items: MutableList<RateItemInfo> = ArrayList()
    private var calculatedBaseValue: Double = 1.0
    private var isBinding: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateItemViewHolder {
        val viewHolder =
            RateItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rate_item, parent, false))
        holders.add(viewHolder)
        return viewHolder
    }

    override fun onBindViewHolder(holder: RateItemViewHolder, position: Int) {
        isBinding = true

        holder.bind(items[position], position)
        holder.itemFocusedCallback = this
        if (position == 0) {
            holder.itemChangedCallback = this
        } else {
            holder.itemChangedCallback = null
        }

        isBinding = false
    }

    override fun getItemCount(): Int = items.size

    fun updateItemsRate(ratesList: List<Pair<String, Double>>) {
        if (ratesList.isNotEmpty()) {
            if (items.isEmpty()) {
                for (rate in ratesList) {
                    items.add(RateItemInfo(rate.first, rate.second, rate.second))
                }
                notifyDataSetChanged()
            } else {
                items[0].rate = ratesList.find { items[0].name == it.first }!!.second
                calculatedBaseValue = 1 / items[0].rate * items[0].value
                for (i in 1 until items.size) {
                    items[i].rate = ratesList.find { items[i].name == it.first }!!.second
                    items[i].value = items[i].rate * calculatedBaseValue
                    notifyItemChanged(i)
                }
            }
        }
    }

    override fun onItemChanged(value: Double) {
        calculatedBaseValue = 1 / items[0].rate * value
        for (i in 1 until items.size) {
            items[i].value = items[i].rate * calculatedBaseValue
            if (!isBinding) {
                notifyItemChanged(i)
            }
        }
    }

    override fun onItemFocused(holder: RateItemViewHolder) {
        moveUp(holder)
    }

    private fun moveUp(viewHolder: RateItemViewHolder) {
        viewHolder.layoutPosition.takeIf { it > 0 }?.also { currentPosition ->
            items.removeAt(currentPosition).also {
                items.add(0, it)
            }
            for (holder in holders) {
                holder.itemChangedCallback = null
            }
            viewHolder.itemChangedCallback = this
            viewHolder.itemPosition = 0
            notifyItemMoved(currentPosition, 0)
        }
    }

    override fun onViewRecycled(holder: RateItemViewHolder) {
        holder.clearCallbacks()
        super.onViewRecycled(holder)
    }

}
