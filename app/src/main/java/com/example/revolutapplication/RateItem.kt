package com.example.revolutapplication

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class RateItemInfo(val name: String, var rate: Double, var value: Double = 1.0)

data class RatesResponse(@SerializedName("base") val base: String = "", @SerializedName("rates") val rates: JsonObject? = null)
